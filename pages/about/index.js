import { Fragment } from 'react';
import { Container, Row, Col, Image } from 'react-bootstrap';

export default function About() {
	return (
		<Fragment>
			<h1 className="my-4">About this App</h1>

			<p>JetTrack is a simple budget tracking web application designed to help users keep track of each of their transactions whether it's an income or expense. It also provides charts for the users' monthly income and monthly expenses so that the users can visualize how much they spend or earn monthly.</p>
			<p>JetTrack is made by co-developers and Zuitt bootcamp graduates, John Lawrence Co and Joeren Christopher Lee, for their third capstone project for the bootcamp. It uses the MERN stack technology as well as Next.js for server-side rendering and other useful JavaScript libraries such as SweeAlert2, Google Authentication and more.</p>

			<h2 className="text-center mb-3">The Developers</h2>

			<Container>
				<Row>
					<Col sm={12} lg={6} className="d-flex flex-column align-items-center my-3">
						<Image src="https://via.placeholder.com/180" rounded/>

						<h4 className="my-3">Joeren Christopher B. Lee</h4>

						<p className="mt-3 mb-1">
							Joeren Christopher B. Lee is a Zuitt Bootcamp graduate. He is a hardworking person. His dream is to further develop his skills as a developer and widen his scope in the tech industry.
						</p>
					</Col>
					<Col sm={12} lg={6} className="d-flex flex-column align-items-center my-3">
						<Image src="https://via.placeholder.com/180" rounded />

						<h4 className="my-3">John Lawrence C. Co</h4>

						<p className="mt-3 mb-1">
							John Lawrence C. Co is also a Zuitt Bootcamp graduate. He forwent his background and knowledge in Civil Engineering to pursue and shift his career path into Web Development. He aspires to become a successful web developer.
						</p>
					</Col>
				</Row>
				
			</Container>

		</Fragment>
		
	);
}