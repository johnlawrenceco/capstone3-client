import { Fragment, useState, useContext, useEffect } from "react";
import { Container, Form, Button } from "react-bootstrap";
import Swal from 'sweetalert2'
import Router from 'next/router'

import UserContext from '../../UserContext';

export default function Register() {
	const {user, setUser} = useContext(UserContext);

	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNo, setMobileNo] = useState('')
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
		if ((email !== '') 
			&& (password1 !== '') 
			&& (password2 !== '')
			&& (password1 === password2)) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password1, password2]);


	function registerUser(e) {
		e.preventDefault();


		setEmail('');
		setFirstName('');
		setLastName('');
		setMobileNo('')
		setPassword1('');
		setPassword2('');

		if((password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNo.length === 11)) {
			fetch('https://guarded-mountain-64853.herokuapp.com/api/users/email-exists', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					email: email
				})
			})
			.then(res => res.json())
			.then(data => {
				if(data === false) {
						fetch('https://guarded-mountain-64853.herokuapp.com/api/users/', {
							method: 'POST',
							headers: {
								'Content-Type': 'application/json'
							}, 
							body: JSON.stringify({
								firstName: firstName,
								lastName: lastName,
								email: email,
								password: password1,
								mobileNo: mobileNo
							})
						})
						.then(res => res.json())
						.then(data => {
							if(data === true) {
								Swal.fire({
  								icon: 'success',
  								title: 'Registration Successful.'
								})
							Router.push('/login')
							} else {
								Swal.fire({
  								icon: 'error',
  								title: 'Registration Failed.'
								})
							} 
						})

				} else {
					Swal.fire({
  						icon: 'error',
  						title: 'Email Already Exists.'
					})
				}
			})
	} else {
		alert('Invalid Input.')
	}
}


    return (
    	<Fragment>
    		<h1 className="my-3">Register</h1>

		    <Form onSubmit={e => registerUser(e)} >
		    	<Form.Group controlId="firstName">
			        <Form.Label>First Name</Form.Label>
			        <Form.Control 
			        	type="text" 
			        	placeholder="Input your First Name" 
			        	value={firstName} 
			        	onChange={e => setFirstName(e.target.value)}
			        	required 
			        />
		        </Form.Group>
		        <Form.Group controlId="lastName">
			        <Form.Label>Last Name</Form.Label>
			        <Form.Control 
			        	type="text" 
			        	placeholder="Input your Last Name" 
			        	value={lastName} 
			        	onChange={e => setLastName(e.target.value)}
			        	required 
			        />
		        </Form.Group>
		        <Form.Group controlId="email">
			        <Form.Label>Email Address</Form.Label>
			        <Form.Control 
			        	type="email" 
			        	placeholder="Email address" 
			        	value={email} 
			        	onChange={e => setEmail(e.target.value)}
			        	required 
			        />
		        </Form.Group>

		        <Form.Group controlId="mobileNo">
			        <Form.Label>Mobile Number</Form.Label>
			        <Form.Control 
			        	type="text" 
			        	placeholder="Mobile Number" 
			        	value={mobileNo} 
			        	onChange={e => setMobileNo(e.target.value)}
			        	required 
			        />
		        </Form.Group>

		        <Form.Group controlId="password1">
			        <Form.Label>Password</Form.Label>
			        <Form.Control 
			        	type="password" 
			        	placeholder="Password" 
			        	value={password1} 
			        	onChange={e => setPassword1(e.target.value)} 
			        	required 
			        />
		        </Form.Group>

		        <Form.Group controlId="password2">
			        <Form.Label>Confirm Password</Form.Label>
			        <Form.Control 
			        	type="password" 
			        	placeholder="Confirm Password" 
			        	value={password2} 
			        	onChange={e => setPassword2(e.target.value)} 
			        	required 
			        />
		        </Form.Group>

		        {
		        	isActive 
		        	? <Button className="button--customize" type="submit">Submit</Button>
		        	: <Button className="button--customize" disabled>Submit</Button>
		        }
		    </Form>
    	</Fragment>
    	
    );
}