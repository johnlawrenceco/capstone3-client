import { Fragment, useEffect, useState, useContext } from 'react';
import UserContext from '../../../UserContext';
import { Button, Card, Form, Jumbotron } from 'react-bootstrap';
import Router from 'next/router';
import Swal from 'sweetalert2';

export default function AddNewCategory() {
	const {user, setUser} = useContext(UserContext);

	const [categoryName, setCategoryName] = useState('');
	const [categoryType, setCategoryType] = useState('');
	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
		if ((categoryType!== '') 
			&& (categoryName !== '')) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [categoryType, categoryName]);

	function addCategory(e) {
		e.preventDefault();

		let token = localStorage.getItem('token');

		if(categoryType !== '' && categoryName !== '') {
			fetch('https://guarded-mountain-64853.herokuapp.com/api/users/categories/new', {
				method: 'POST',
				headers: {
					'Authorization':  `Bearer ${token}`,
					'Content-Type': 'application/json'

				},
				body: JSON.stringify({
					categoryType: categoryType,
					categoryName: categoryName
				})
			})
			.then(res => res.json())
			.then(data => {

				if(data) {
					Swal.fire({
  					icon: 'success',
  					title: 'Category Successfully Added.'
					})

					Router.push('/user/categories')
				} else {
					Swal.fire({
  					icon: 'success',
  					title: 'Category Failed to Add.'
					})
				}

				//clear inputs after fetching
				setCategoryName('');
				setCategoryType('');

				Router.push
			})
		} else {
			alert('Invalid Input.')
		}
	}

	return (
		user.email
		?
		<Fragment> 
			<h1 className="my-3">Add New Category</h1>

			<Card className="text-white card--new">
			  	<Card.Header>Category Information</Card.Header>
			  	<Card.Body>
			    	<Form onSubmit={e => addCategory(e)}>
			    			<Form.Group controlId="category">
			    			    <Form.Label>Category Name</Form.Label>
			    			    <Form.Control 
			    			    	type="text" 
			    			    	placeholder="Enter category name" 
			    			    	value={categoryName} 
			    			    	onChange={e => setCategoryName(e.target.value)}
			    			    />
			    			  </Form.Group>
			    		<Form.Group>
			    			<Form.Label>Category Type:</Form.Label>
			    			<Form.Control 
			    				as="select" 
			    				value={categoryType} 
			    				onChange={e => setCategoryType(e.target.value)}
			    			>
			    				<option value="">Select Category</option>
			    			  	<option value="Income">Income</option>
			    			  	<option value="Expenses">Expenses</option>
			    			</Form.Control>
			    		</Form.Group>

				        {
		    				isActive 
		    				? <Button className="button--customize" type="submit">Submit</Button>
		    				: <Button className="button--customize" disabled>Submit</Button>
		    			}
			    	</Form>
			    </Card.Body>
			</Card>
		</Fragment>
		:
		<Jumbotron>
			<h1>404: PAGE NOT FOUND</h1>
		</Jumbotron>
	);
}