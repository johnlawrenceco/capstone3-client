import { Fragment, useState, useContext, useEffect } from 'react';
import UserContext from '../../../UserContext';
import Router from 'next/router';
import { Button, Table, Jumbotron } from 'react-bootstrap';



export default function Categories() {
	const {user, setUser} = useContext(UserContext);

	const [categories, setCategories] = useState([]);

	useEffect(() => {
		let token = localStorage.getItem('token');

		fetch(`https://guarded-mountain-64853.herokuapp.com/api/users/categories`, {
			headers: {
				'Authorization':  `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setCategories(data.map(category => {
				return (
				  	<tr key={category._id}>
					    <td>{category.categoryName}</td>
					    <td>{category.categoryType}</td>
				  	</tr>
				)
			}))
		})
	}, [categories])

	return (
		user.email
		?
			<Fragment> 
				<h1 className="my-3">Categories</h1>

				<Button 
					className="mb-3 button--customize" 
					onClick={() => 
						Router.push('./categories/new')
					}
				>Add New Category</Button>

				<Table striped bordered hover>
				   	<thead>
				    	<tr>
				      		<th>Category</th>
				      		<th>Type</th>
				    	</tr>
				  	</thead>

				  	<tbody>
				  		{categories}
				  	</tbody>
				  	
				</Table>
			</Fragment>
		:
			<Jumbotron>
				<h1>404: PAGE NOT FOUND</h1>
			</Jumbotron>
	);
}