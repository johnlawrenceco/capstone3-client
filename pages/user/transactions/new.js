import { Fragment, useEffect, useState, useContext } from 'react';
import UserContext from '../../../UserContext';
import { Card, Form, Button, Jumbotron } from 'react-bootstrap';
import Router from 'next/router'
import Swal from 'sweetalert2'

export default function newTransaction() {
	
	const {user} = useContext(UserContext);
	const [categoryName, setCategoryName] = useState('Eat')
	const [categoryType, setCategoryType] = useState('')
	const [amount, setAmount] = useState(0)
	const [description, setDescription] = useState('')
	const [isActive, setIsActive] = useState(false)
	const [categories, setCategories] = useState('');
	

	useEffect(() => {
		let token = localStorage.getItem('token');

		fetch(`https://guarded-mountain-64853.herokuapp.com/api/users/categories`, {
			headers: {
				'Authorization':  `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if (categoryType === "Income") {

				const incomeCategory = data.filter(category => category.categoryType === "Income");

				setCategories(incomeCategory.map(category => {
					return (
						<option value={category.categoryName}>{category.categoryName}</option>
					);
				}))
			} else if (categoryType === "Expenses") {

				const expensesCategory = data.filter(category => category.categoryType === "Expenses");

				setCategories(expensesCategory.map(category => {
					return (
						<option value={category.categoryName}>{category.categoryName}</option>
					);
				}))
			} else {
				return null
			}
		})

	}, [categoryType])

	useEffect(() => {
		if ((categoryType !== '') 
			&& (categoryName !== '')  
			&& (amount !== 0)
			&& (description !== '')) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [categoryType, amount, description]);


	function addNewTransaction(e) {

		e.preventDefault();

		let token = localStorage.getItem('token')


		if(categoryType !== '' 
			&& categoryName !== ''
			&& description !== '' 
		) {
			if (amount <= 0) {
				Swal.fire({
				  icon: 'warning',
				  title: 'Invalid Input',
				  text: 'Amount can\'t be 0 or less!'
				})
			} else {
				fetch('https://guarded-mountain-64853.herokuapp.com/api/users/transactions/new', {
					method: 'POST',
					headers: {
						'Authorization':  `Bearer ${token}`,
						'Content-Type': 'application/json'

					},
					body: JSON.stringify({
						categoryType: categoryType,
						categoryName: categoryName,
						amount: amount,
						description: description,
					})
				})
				.then(res => res.json())
				.then(data => {
					if(data) {
						Swal.fire({
	  					icon: 'success',
	  					title: 'Transaction successfully added.'
						})
						Router.push('/user/transactions')
					} else {
						Swal.fire({
	  					icon: 'warning',
	  					title: 'Something went wrong! Please Try again.'
						})
					}
				})
				
				setCategoryType('')
				setCategoryName('')
				setAmount('')
				setDescription('')
			}
		} else {
			Swal.fire({
				icon: 'error',
				title: 'Invalid Input',
				text: 'Please make sure to fill all inputs.'
			})
		}
	}
	
	return (
		user.email
		?
		<Fragment>
			<h1 className="my-3">New Transaction</h1>

			<Card className="text-white card--new">
			  	<Card.Header className="font-weight-bold">Transaction Information</Card.Header>
			  	<Card.Body>
			    	<Form onSubmit={e => addNewTransaction(e)}>
			    		<Form.Group>
			    			<Form.Label>Category Type:</Form.Label>
			    			<Form.Control as="select" value={categoryType} onChange={e => setCategoryType(e.target.value)}>
		    					<option value="">Select Category Type</option>
		    				  	<option value="Income">Income</option>
		    				  	<option value="Expenses">Expenses</option>
			    			</Form.Control>
			    		</Form.Group>

			    		<Form.Group>
			    			<Form.Label>Category Name:</Form.Label>
			    			<Form.Control as="select" value={categoryName} onChange={e => setCategoryName(e.target.value)}>
			    				<option value="">Select Category Name</option>
			    				{categories}
			    			</Form.Control>
			    		</Form.Group>

			    		<Form.Group controlId="number">
					        <Form.Label>Amount</Form.Label>
					        <Form.Control
					        	type="number"
					        	value={amount}
					        	onChange={e => setAmount(e.target.value)}
					        	placeholder="0"
					        />
				        </Form.Group>

				        <Form.Group controlId="description">
				            <Form.Label>Description</Form.Label>
				            <Form.Control as="textarea" rows={3} value={description} onChange={e => setDescription(e.target.value)}/>
				          </Form.Group>
				        {
	        				isActive 
	        				? <Button className="button--customize" type="submit">Submit</Button>
	        				: <Button className="button--customize" disabled>Submit</Button>
	        			}
			    	</Form>	
			    </Card.Body>
			</Card>
		</Fragment>
		:
		<Jumbotron>
			<h1>404: PAGE NOT FOUND</h1>
		</Jumbotron>
	);
}