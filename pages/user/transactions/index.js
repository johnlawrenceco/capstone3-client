import { Fragment, useState, useContext, useEffect } from 'react'
import UserContext from '../../../UserContext'
import Router from 'next/router'
import { Card, Form, ListGroup, Row, Col, Button, Table, Jumbotron } from 'react-bootstrap'
import moment from 'moment'
 
export default function TransactionsTry() {
    const {user} = useContext(UserContext)

    const [transactions, setTransactions] = useState('')
    const [categoryType, setCategoryType] = useState('All')
    const [categoryName, setCategoryName] = useState('')
    const [searchDesc, setSearchDesc] = useState('')
    const [currentBalance, setCurrentBalance] = useState(0)

    //This useEffect waits for categoryType changes (change in value in dropdown) and search value changes
    useEffect(() => {
            let token = localStorage.getItem('token');

            fetch(`https://guarded-mountain-64853.herokuapp.com/api/users/transactions`, {
                headers: {
                        'Authorization':  `Bearer ${token}`
                }
            })
            .then(res => res.json())
            .then(data => {
                const allRecords = data

                const reversedData = allRecords.reverse() //so that the latest transaction displays first

                const amountIdArray = makeAmountIdArray(data)

                console.log(amountIdArray)

                const totalAmoutForThisElemArray = produceTotalAmountAndIdForThisElemArray(amountIdArray)

                console.log(totalAmoutForThisElemArray)

                switch (categoryType) {
                    case "All":
                        setTransactions(filterWithSearch(searchDesc, reversedData)
                            .map(transaction => {
                                return (
                                    <Fragment>
                                        <Card className="my-3">
                                            <Card.Body>
                                                <Card.Title className="text-center font-weight-bold text-white">
                                                    {transaction.description}
                                                </Card.Title>
                                                <Card.Subtitle className="text-center text-white">
                                                    {transaction.categoryName}
                                                </Card.Subtitle>
                                            </Card.Body>
                                            <ListGroup className="list-group-flush">
                                                <ListGroup.Item className="text-center my-0 list-group__amount">
                                                    {
                                                        transaction.categoryType === "Income" 
                                                            ? <p>Gained: ₱{transaction.amount} ({transaction.categoryType})</p>
                                                            : <p>Lost: ₱{transaction.amount} ({transaction.categoryType})</p>
                                                    }
                                                </ListGroup.Item>
                                                <ListGroup.Item className="font-weight-bold text-center list-group__total-after">
                                                    Total After: ₱{totalAmoutForThisElemArray
                                                                .find(transactionInThisArray => transactionInThisArray.id === transaction._id)
                                                                .totalAmountForThisElem
                                                    }
                                                </ListGroup.Item>
                                            </ListGroup>
                                            <Card.Footer>
                                                <small className="text-white">
                                                    Added last {moment(transaction.transactionDate).format('MMMM Do YYYY, h:mm a')}
                                                </small>
                                            </Card.Footer>
                                        </Card>
                                    </Fragment>
                                )
                            })
                        )
                        break;

                    case "Income":
                        const incomeTransactions = reversedData.filter(transaction => transaction.categoryType === "Income")

                        setTransactions(filterWithSearch(searchDesc, incomeTransactions)
                            .map(transaction => {
                                return (
                                    <Fragment>
                                        <Card className="my-3">
                                            <Card.Body>
                                                <Card.Title className="text-center font-weight-bold text-white">
                                                    {transaction.description}
                                                </Card.Title>
                                                <Card.Subtitle className="text-center text-white">
                                                    {transaction.categoryName}
                                                </Card.Subtitle>
                                            </Card.Body>
                                            <ListGroup className="list-group-flush">
                                                <ListGroup.Item className="text-center my-0 list-group__amount">
                                                    {
                                                        transaction.categoryType === "Income" 
                                                            ? <p>Gained: ₱{transaction.amount} ({transaction.categoryType})</p>
                                                            : <p>Lost: ₱{transaction.amount} ({transaction.categoryType})</p>
                                                    }
                                                </ListGroup.Item>
                                                <ListGroup.Item className="font-weight-bold text-center list-group__total-after">
                                                    Total After: ₱{totalAmoutForThisElemArray
                                                                .find(transactionInThisArray => transactionInThisArray.id === transaction._id)
                                                                .totalAmountForThisElem
                                                    }
                                                </ListGroup.Item>
                                            </ListGroup>
                                            <Card.Footer>
                                                <small className="text-white">
                                                    Added last {moment(transaction.transactionDate).format('MMMM Do YYYY, h:mm a')}
                                                </small>
                                            </Card.Footer>
                                        </Card>
                                    </Fragment>
                                )
                            })
                        )
                        break;

                    case "Expenses":
                        const expensesTransactions = reversedData.filter(transaction => transaction.categoryType === "Expenses")

                        setTransactions(filterWithSearch(searchDesc, expensesTransactions)
                            .map(transaction => {
                                return (
                                    <Fragment>
                                        <Card className="my-3">
                                            <Card.Body>
                                                <Card.Title className="text-center font-weight-bold text-white">
                                                    {transaction.description}
                                                </Card.Title>
                                                <Card.Subtitle className="text-center text-white">
                                                    {transaction.categoryName}
                                                </Card.Subtitle>
                                            </Card.Body>
                                            <ListGroup className="list-group-flush">
                                                <ListGroup.Item className="text-center my-0 list-group__amount">
                                                    {
                                                        transaction.categoryType === "Income" 
                                                            ? <p>Gained: ₱{transaction.amount} ({transaction.categoryType})</p>
                                                            : <p>Lost: ₱{transaction.amount} ({transaction.categoryType})</p>
                                                    }
                                                </ListGroup.Item>
                                                <ListGroup.Item className="font-weight-bold text-center list-group__total-after">
                                                    Total After: ₱{totalAmoutForThisElemArray
                                                                .find(transactionInThisArray => transactionInThisArray.id === transaction._id)
                                                                .totalAmountForThisElem
                                                    }
                                                </ListGroup.Item>
                                            </ListGroup>
                                            <Card.Footer>
                                                <small className="text-white">
                                                    Added last {moment(transaction.transactionDate).format('MMMM Do YYYY, h:mm a')}
                                                </small>
                                            </Card.Footer>
                                        </Card>
                                    </Fragment>
                                )
                            })
                        )
                        break;
                }
            })
    }, [categoryType, searchDesc])
    console.log(transactions)

    //This useEffect waits for changes in number of transactions to display current balance
    useEffect(() => {
        let token = localStorage.getItem('token');

        fetch(`https://guarded-mountain-64853.herokuapp.com/api/users/transactions`, {
            headers: {
                    'Authorization':  `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            const amountArray = makeAmountIdArray(data).map(transaction => transaction.amount)

            setCurrentBalance(amountArray.reduce((accumulator, currentValue) => accumulator + currentValue, 0))
        })
    }, [transactions])

    //function for filtering with searching; used inside the setTransactions setter functions inside the 1st useEffect 
    function filterWithSearch(search, transactions) {
        const filteredTransactions = transactions.filter(transaction => {
                return transaction.description.toLowerCase().includes(search.toLowerCase());
        })

        return search === '' ? transactions : filteredTransactions;
    }

    function makeAmountIdArray(transactions) {
            //Deep copy using JSON.parse(JSON.stringify(<object/array>)) so that the original array doesn't get affected (mainly for not affecting displaying with signs of amount)
        const copiedTransactions = JSON.parse(JSON.stringify(transactions)) 

        return copiedTransactions.map(transaction => {
            if (transaction.categoryType === "Income") {
                transaction.amount *= 1
            } else if (transaction.categoryType === "Expenses") {
                transaction.amount *= -1
            } else {
                throw("There's something wrong.")
            }

            return {
                id: transaction._id,
                amount: transaction.amount
            } 
        })
    }

    function produceTotalAmountAndIdForThisElemArray(amountIdArray) {
        const totalAmountAndIdForThisElemArray = [];

        const amountArray = [];
        
        for (const transaction of amountIdArray.reverse()) {

            amountArray.push(transaction.amount)

            totalAmountAndIdForThisElemArray.push({
                id: transaction.id,
                totalAmountForThisElem: amountArray.reduce((accumulator, currentValue) => accumulator + currentValue, 0)
            })
        }

        return totalAmountAndIdForThisElemArray.reverse()
    }

    return (
        user.email
        ?
        <Fragment>
            <h1 className="my-3">Transactions</h1>
            <h3>Current Balance: ₱{currentBalance}</h3>
            <Form>
                <Row>
                    <Col xs={12} md={3}>
                        <Button
                            className= 'mt-3 button--customize'
                            onClick={() => Router.push('./transactions/new')}
                        >Add New Transaction</Button>
                    </Col>

                    <Col xs={12} md={3}>
                        <Button
                            className= 'mt-3 button--customize'
                            onClick={() => Router.push('./categories/new')}
                        >Add New Category</Button>
                    </Col>

                    <Col xs={12} md={4}>
                        <Form.Control
                            className= 'mt-3'
                            type='text'
                            placeholder="Search Description"
                            value={searchDesc}
                            onChange={e => setSearchDesc(e.target.value)}
                        />
                    </Col>


                    <Col xs={12} md={2}>
                        <Form.Control
                            className= 'mt-3'
                            as="select"
                            value={categoryType}
                            onChange={e => setCategoryType(e.target.value)}
                        >
                            <option value="All">All</option>
                            <option value="Income">Income</option>
                            <option value="Expenses">Expenses</option>
                        </Form.Control>
                    </Col>
                </Row>
            </Form>

                {transactions}
        </Fragment>
        :
            <Jumbotron>
                    <h1>404: PAGE NOT FOUND</h1>
            </Jumbotron>
    )
}