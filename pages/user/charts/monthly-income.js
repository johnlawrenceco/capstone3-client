import {Fragment, useState, useEffect} from 'react'
import {Row, Col, Alert} from 'react-bootstrap'
import MonthlyIncome from '../../../components/MonthlyIncome'
import Head from 'next/head'

export default function monthlyincome() {

	const [incomeData, setIncomeData] = useState([])

	useEffect(() => {

		let token = localStorage.getItem('token');

		fetch(`https://guarded-mountain-64853.herokuapp.com/api/users/transactions`, {
			headers: {
				'Authorization':  `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setIncomeData(data.filter(data => {
			return data.categoryType === 'Income'
			}))
		})
	}, [incomeData])




	return (

		<Fragment>
			<Head>
				<title>
					Monthly Income in PHP
				</title>
			</Head>

			<h1 className="my-3">Monthly Income</h1>

			<Row>
				<Col xs={12} md={12}>
					<MonthlyIncome incomeData={incomeData}/>
				</Col>
			</Row>
		</Fragment>

		)

}