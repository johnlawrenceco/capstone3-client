import {Fragment, useState, useEffect} from 'react'
import {Row, Col, Form, Button} from 'react-bootstrap'
import CategoryBreakdown from '../../../components/CategoryBreakdown'
import moment from 'moment'
 
export default function categoryBreakdown() {
 
    const [categoryData, setCategoryData] = useState([])
    const [transactionDate, setTransactionDate] = useState('')
    const [dateFrom, setDateFrom] = useState(moment(new Date()).subtract(1, 'month').format('YYYY-MM-DD'))
    const [dateTo, setDateTo] = useState(moment(new Date()).format('YYYY-MM-DD'))
    const [dateToCheck, setDateToCheck] = useState([])

    useEffect(() => {

        let token = localStorage.getItem('token');

        fetch('https://guarded-mountain-64853.herokuapp.com/api/users/transactions', {
            headers: {
                'Authorization':  `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            setDateToCheck(data.filter(transaction => {
                if (( moment(transaction.transactionDate).isSameOrAfter(dateFrom, "day") 
                        && moment(dateTo).isSameOrAfter(transaction.transactionDate, "day") )) {
                    return moment(transaction.transactionDate).format('YYYY-MM-DD')
                }
            }))
        })
 
    }, [dateFrom, dateTo])

    return (
        <Fragment>
            <h1 className="my-3">Category Breakdown</h1>

            <Form>
                <Row>
                    <Col xs={12} md={6}>
                        <Form.Group>
                        <h3>From</h3>
                            <Form.Control type='date' value={dateFrom} onChange={e => setDateFrom(e.target.value)}>
                            </Form.Control>
                        </Form.Group>
                    </Col>

                    <Col xs={12} md={6}>
                        <h3>To</h3>
                        <Form.Control type='date' value={dateTo} onChange={e => setDateTo(e.target.value)}>
                        </Form.Control>
                    </Col>
                </Row>
            </Form>
    		
       		 <Row>
                <Col xs={12} md={12}>
                    <CategoryBreakdown rawData={dateToCheck}/>
                </Col>
       		 </Row>
        </Fragment>
    )
 
}