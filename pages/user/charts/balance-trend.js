import {Line} from 'react-chartjs-2';
import {Fragment, useState, useEffect} from 'react'
import {Row, Col, Alert, Form, Button} from 'react-bootstrap'
import BalanceTrend from '../../../components/BalanceTrend'
import moment from 'moment'

export default function balanceTrend() {

    const [currentBalance, setCurrentBalance] = useState(0)
    const [transactions, setTransactions] = useState([])
    const [dateFrom, setDateFrom] = useState(moment(new Date()).subtract(1, 'month').format('YYYY-MM-DD'))
    const [dateTo, setDateTo] = useState(moment(new Date()).format('YYYY-MM-DD'))
    const [dateToCheck, setDateToCheck] = useState([])

    useEffect(() => {
        let token = localStorage.getItem('token');

        fetch(`https://guarded-mountain-64853.herokuapp.com/api/users/transactions`, {
            headers: {
                    'Authorization':  `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {

            const amountIdArray = makeAmountIdArray(data)

            // console.log(amountIdArray)

            const totalAmountForThisElemArray = produceTotalAmountAndIdForThisElemArray(amountIdArray)

        

            setTransactions(data.map(transaction => {

                return (
                    totalAmountForThisElemArray
                    .find(transactionInThisArray => transactionInThisArray.id === transaction._id)
                )
            }))
        })
    }, [])

    useEffect(() => {
        let token = localStorage.getItem('token');

        fetch(`https://guarded-mountain-64853.herokuapp.com/api/users/transactions`, {
            headers: {
                    'Authorization':  `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            const amountArray = makeAmountIdArray(data).map(transaction => transaction.amount)

            setCurrentBalance(amountArray.reduce((accumulator, currentValue) => accumulator + currentValue, 0))
        })
    }, [transactions])
    
    function makeAmountIdArray(transactions) {
        const copiedTransactions = JSON.parse(JSON.stringify(transactions))

        return copiedTransactions.map(transaction => {
            if (transaction.categoryType === "Income") {
                transaction.amount *= 1
            } else if (transaction.categoryType === "Expenses") {
                transaction.amount *= -1
            } else {
                throw("There's something wrong.")
            }

            return {
                id: transaction._id,
                amount: transaction.amount,
                transactionDate: transaction.transactionDate
            } 
        })
    }

    useEffect(() => {
        setDateToCheck(transactions.filter(transaction => {
                if (( moment(transaction.transactionDate).isSameOrAfter(dateFrom, "day") 
                        && moment(dateTo).isSameOrAfter(transaction.transactionDate, "day") )) {
                    return moment(transaction.transactionDate).format('YYYY-MM-DD')
                }
        }))
        
    }, [dateFrom, dateTo])

    console.log(dateToCheck)
    function produceTotalAmountAndIdForThisElemArray(amountIdArray) {
        const totalAmountAndIdForThisElemArray = [];

        const amountArray = [];
        
        for (const transaction of amountIdArray) {

            amountArray.push(transaction.amount)

            totalAmountAndIdForThisElemArray.push({
                id: transaction.id,
                totalAmountForThisElem: amountArray.reduce((accumulator, currentValue) => accumulator + currentValue, 0),
                transactionDate: transaction.transactionDate
            })
        }

        return totalAmountAndIdForThisElemArray
    }

    
    return (
        <Fragment> 
            <h1 className="my-3">Balance Trend</h1>
            <Form>
            <Row>
                <Col xs={12} md={6}>
                    <Form.Group>
                    <h3>From</h3>
                        <Form.Control type='date' value={dateFrom} onChange={e => setDateFrom(e.target.value)}>
                        </Form.Control>
                    </Form.Group>
                </Col>
                <Col xs={12} md={6}>
                    <h3>To</h3>
                    <Form.Control type='date' value={dateTo} onChange={e => setDateTo(e.target.value)}>
                    </Form.Control>
                </Col>
            </Row>
            
            </Form>
            {
                <Row>
                    <Col xs={12} md={12}>
                        <BalanceTrend balanceTrend={dateToCheck}/>
                    </Col>
                </Row>

            }
            
        </Fragment>
        )
}