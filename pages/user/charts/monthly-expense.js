import {Fragment, useState, useEffect} from 'react'
import {Row, Col, Alert} from 'react-bootstrap'
import MonthlyExpenses from '../../../components/MonthlyExpenses'
import Head from 'next/head'

export default function monthlyexpenses() {

	const [expensesData, setExpensesData] = useState([])

	useEffect(() => {

		let token = localStorage.getItem('token');

		fetch(`https://guarded-mountain-64853.herokuapp.com/api/users/transactions`, {
			headers: {
				'Authorization':  `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setExpensesData(data.filter(data => {
			return data.categoryType === 'Expenses'
			}))
		})
	}, [expensesData])




	return (

		<Fragment>
			<Head>
				<title>
					Monthly Expenses in PHP
				</title>
			</Head>

			<h1 className="my-3">Monthly Expenses</h1>

			<Row>
				<Col xs={12} md={12}>
					<MonthlyExpenses expensesData={expensesData}/>
				</Col>
			</Row>
		</Fragment>

		)

}