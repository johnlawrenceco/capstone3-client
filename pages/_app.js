import { Fragment, useState, useEffect } from 'react';
import Head from 'next/head'
import { Container } from 'react-bootstrap';
import { UserProvider } from '../UserContext';
import NavBar from '../components/NavBar';
import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';


function MyApp({ Component, pageProps }) {
  	//Create global user state
  	const [user, setUser] = useState({
      id: null,
  		email: null,
  	});
  	
  	useEffect(() => {
  	
        fetch('https://guarded-mountain-64853.herokuapp.com/api/users/details', {
           headers: {
           Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      	})
      	.then(res => res.json())
      	.then(data => {

	        //If no data, set the user state back to null.
	        if (data._id) {
	          	setUser({
	            	id: data._id,
	           		email: data.email
	          	});
	        } else {
	          	setUser({
	            	id: null,
	            	email: null
	          	});
	        }
      });
  	}, []);

  	//clears the localStorage when logging out
  	const unsetUser = () => {
  		localStorage.clear();

  		setUser({
  			id: null,
  			email: null
  		});
  	}

    return (
      <Fragment>
        <Head>
          <link rel="preconnect" href="https://fonts.gstatic.com" />
          <link href="https://fonts.googleapis.com/css2?family=Dosis:wght@800&family=Raleway:wght@500&family=Montserrat&display=swap" 
            rel="stylesheet" 
          />
        </Head>
        <UserProvider value={{user, setUser, unsetUser}}>
          <NavBar />
          <Container>
            <Component {...pageProps} />
          </Container>
        </UserProvider>
      </Fragment>
      
  	);
}

export default MyApp
