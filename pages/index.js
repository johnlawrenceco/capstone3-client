import { Fragment, useContext, useEffect } from 'react'
import Head from 'next/head'
import {useRouter} from 'next/router'
import UserContext from '../UserContext';
import { Container } from 'react-bootstrap'


export default function Home() {
  const {user} = useContext(UserContext)

  const router = useRouter()

  useEffect(() => {
      if (user.email) {
        router.push('/user/transactions')
      } else {
      	router.push('/login')
      }
    }, [user])

  return (
    <Fragment>
    	<Head>
    		<title>Redirecting...</title>
    	</Head>

    	<Container>
    		<h1 classname="my-3">Redirecting...</h1>
    	</Container>
    	
    </Fragment>
  )
}
