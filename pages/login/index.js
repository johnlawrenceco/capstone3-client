import { Fragment, useState, useContext, useEffect } from "react";
import { Container, Form, Row, Button } from "react-bootstrap";
import Swal from 'sweetalert2'
import Router from 'next/router'
import UserContext from '../../UserContext';
import {GoogleLogin} from 'react-google-login'

export default function Login() {
	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);
	const [willRedirect, setWillRedirect] = useState(false)

	useEffect(() => {
		if (email !== '' && password !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password]);


	//for google Login authenticate later
	const retrieveUserDetails = (accessToken) => {
		//add the URL to get the user detail but with the use of our App helper
		//http://localhost:4000/api
		const options = {
			headers: {
				Authorization: `Bearer ${accessToken}`
			}
		}
		//CHANGE TO APPROPRIATE PORT and PATH
		fetch(`https://guarded-mountain-64853.herokuapp.com/api/users/details`, options)
		.then(res => res.json())
		.then(data => {
			setUser({
				id: data._id,
				email: data.email,
			});

			Router.push('/user/transactions')
		});
	};

	function authenticate(e){
		e.preventDefault();

		//CHANGE port or path IF NOT 4000 or different path
		fetch(`https://guarded-mountain-64853.herokuapp.com/api/users/login`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data.accessToken) {
				localStorage.setItem('token', data.accessToken);

				
				//change if not 4000
				fetch('https://guarded-mountain-64853.herokuapp.com/api/users/details', {
					headers: {
						'Authorization': `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					setUser({
						id: data._id,
						email: data.email
					});
					Swal.fire({
					icon: 'success',
					title: 'Successful Login'
					})
					Router.push('/user/transactions');
					

				});
			} else {
				Swal.fire({
				icon: 'error',
				title: 'The email and password did not match our records. Please double-check and try again.'
				})
			}

		//clear input fields
		setEmail('');
		setPassword('');
			
		});
	}

function authenticateGoogleToken(response) {
		
		const payload = {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				tokenId: response.tokenId,
				accessToken: response.accessToken
			})
		}

		console.log(response)

		fetch('https://guarded-mountain-64853.herokuapp.com/api/users/verify-google-id-token', payload)
		.then(res => res.json())
		.then(data => {
			// to show alerts if the user logged in properly or there are errors.
			if(typeof data.accessToken !== 'undefined') {
				// set the accessToken into our local storage.
				localStorage.setItem('token', data.accessToken)
				// sweet alert to show user's successful login
				Swal.fire({
					icon: 'success',
					title: 'Successful Login'
				})
				// retrieve user details then redirect to courses
				retrieveUserDetails(data.accessToken)
			} else {
				if (data.error == 'google-auth-error') {
					Swal.fire({
						icon: 'error',
						title: 'Google Authentication Failed.'
					})
				} else if (data.error === 'login-type-error'){
					Swal.fire({
						icon: 'error',
						title: 'You may have registered through a different login procedure.'
					})
				}
			}
		})
	}


    return (
    	willRedirect === true
		?
		Router.push('/user/transactions')
		:
		<Fragment>
			<h1 className="my-3">Log In</h1>
			
	        <Form onSubmit={e => authenticate(e)}>
		        <Form.Group controlId="email">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control 
			        	type="email" 
			        	placeholder="Email address" 
			        	value={email} 
			        	onChange={e => setEmail(e.target.value)}
			        	required 
			        />
		        </Form.Group>

		        <Form.Group controlId="password">
			        <Form.Label>Password</Form.Label>
			        <Form.Control 
			        	type="password" 
			        	placeholder="password" 
			        	value={password} 
			        	onChange={e => setPassword(e.target.value)} 
			        	required 
			        />
		        </Form.Group>

		        <Row>
		        	{
		        		isActive 
		        		? <Button type="submit" className="mx-auto mt-0 mb-2 button--customize">Submit</Button>
		        		: <Button className="mx-auto mt-0 mb-2 button--customize" disabled>Submit</Button>
		        	}
		        </Row>
		        
	        </Form>

	        <Row className="w-25 mx-auto my-2">
	        	        <GoogleLogin
	        				// to access OAuth Google API 2.0 client.
	        				clientId='204446368530-hec7o84r89pvtrcdq61ff215pn3tau9g.apps.googleusercontent.com'
	        				// 
	        				buttonText = 'Login Using Google'
	        				cookiePolicy = {'single_host_origin'}
	        				onSuccess={authenticateGoogleToken}
	        				onFailure={authenticateGoogleToken}
	        				className = 'w-100 text-center d-flex justify-content-center'
	        			/>
	        </Row>
		</Fragment>
    	
    )
	
}