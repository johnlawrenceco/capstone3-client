//TODO: add useContext

import { useEffect, useContext } from 'react';

import Router from 'next/router';

import UserContext from '../../UserContext';

export default function Logout() {

	const {setUser, unsetUser} = useContext(UserContext);

	useEffect(() => {
		unsetUser();

		Router.push('/login');
	});

	return (
		<h1>Logging out...</h1>
	);
}