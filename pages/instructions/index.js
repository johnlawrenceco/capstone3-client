import { Fragment } from 'react'

export default function Instructions() {
    return (
        <Fragment>
            <h2>Instructions for using this Web Application</h2>
            <h3>For first time users, please follow these instructions:</h3>
            <ol>
                <li>First and foremost, please register and log in or log in via Google Login. When registering, please make sure the mobile number is 11 digits.</li>
                <li>Go to Category and add a new category. First add an Income-type category. This will serve as your initial budget.</li>
                <ul>
                    <li>Make the Category Name a general name like "Travel", "Buy Item", "Sell Item", etc. as these categories can be reused for adding new transactions.</li>
                </ul>
                <li>Continue adding more categories or go to Transactions, then click the "Add New Transaction" button.</li>
                <li>In the Add New Transaction page, if your transactions are empty in the previous Transactions page, make sure to first add an Income-type transaction so this can serve as your intial budget</li>
                <ul>
                    <li>Choose a category (Income first) and add a specific short description/name in the Description input box.</li>
                </ul>
                <li>Continue adding more transactions (income or expenses) in the page or go to your Transactions page to see the records of the transactions you just created and see your Current Balance.</li>
                <li>Explore other pages like the Monthly Income, Monthly Expenses, Balance Trend and Cateogry Breakdown to see different charts.</li>
            </ol>
        </Fragment>
    )
    
}