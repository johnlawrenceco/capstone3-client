import {useState, useEffect} from 'react'
import {Bar} from 'react-chartjs-2'
import moment from 'moment' 

export default function BarChart({expensesData}) {

	const [months, setMonths] = useState([])
	const [monthlyExpenses, setMonthlyExpenses] = useState([])

	const monthsRef = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'November', 'December']

	useEffect(() => {

		setMonths(monthsRef)

		setMonthlyExpenses(months.map(month => {

			let expenses = 0
			
			expensesData.forEach(element => {

				if(moment(element.transactionDate).format('MMMM') === month) {

					expenses = expenses + parseInt(element.amount)

				}

			})

			return expenses

		}))
	}, [months])


	const data = {

		labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'November', 'December'],
		datasets: [{

			label: 'Monthy Expense in PHP',
			backgroundColor: 'rgb(0,255,255)',
			borderColor: 'rgb(0,0,255)',
			borderWidth: 1,
			hoverBackgroundColor: 'rgba(255, 99, 132, 0.4)',
			hoverBorderColor: 'rgba(255, 99, 132, 1)',
			data: monthlyExpenses

		}]


	}

	return (

		<Bar data={data} />

		)

	}

	


