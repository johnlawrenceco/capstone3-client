import {useState, useEffect} from 'react'
import {Bar} from 'react-chartjs-2'
import moment from 'moment' 

export default function BarChart({incomeData}) {

	console.log(incomeData);

	const [months, setMonths] = useState([])
	const [monthlyIncome, setMonthlyIncome] = useState([])

	const monthsRef = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'November', 'December']

	useEffect(() => {

		setMonths(monthsRef)

		setMonthlyIncome(months.map(month => {

			let income = 0

			incomeData.forEach(element => {

				if(moment(element.transactionDate).format('MMMM') === month) {

					income = income + parseInt(element.amount)

				}

			})

			return income

		}))
	}, [months])

	console.log(monthlyIncome);

	const data = {

		labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'November', 'December'],
		datasets: [{

			label: 'Monthy Income in PHP',
			backgroundColor: 'rgb(0,255,255)',
			borderColor: 'rgb(0,0,255)',
			borderWidth: 1,
			hoverBackgroundColor: 'rgba(255, 99, 132, 0.4)',
			hoverBorderColor: 'rgba(255, 99, 132, 1)',
			data: monthlyIncome

		}]


	}

	return (

		<Bar data={data} />

		)

	}

	


