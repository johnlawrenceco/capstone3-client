import {useState, useEffect} from 'react'
import {Line} from 'react-chartjs-2'

export default function LineChart({balanceTrend}) {

	const [balanceTrendAmount, setBalanceTrendAmount] = useState(0)
	
	useEffect(() => {
		setBalanceTrendAmount(balanceTrend.map(element => element.totalAmountForThisElem))
	}, [balanceTrend])

	
	const data = {
		labels: balanceTrendAmount,
		datasets: [{
			data: balanceTrendAmount,
			label: 'Balance Trend',
			backgroundColor: 'rgb(0,255,255)',
			borderColor: 'rgb(0,0,255)',
			borderWidth: 1,
			hoverBackgroundColor: 'rgba(255, 99, 132, 0.4)',
			hoverBorderColor: 'rgba(255, 99, 132, 1)',
		}]
	}

	return (
		<Line data={data} />
		)
	}