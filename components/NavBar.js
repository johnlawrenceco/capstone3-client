import { Fragment, useContext } from 'react';
import { Nav, Navbar } from 'react-bootstrap';
import Link from 'next/link';
import UserContext from '../UserContext';

export default function NavBar() {

	const {user} = useContext(UserContext);
	return (
			<Navbar expand="lg" sticky="top" className="shadow py-1">
				<Link href='/'>
					<a className="navbar-brand disabled">JetTrack</a>
				</Link>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse>
					{
						user.email 
						?
						<Fragment>
							<Nav className="nav-no-logout d-flex justify-content-center align-items-center text-center" style={{ flex: 1}}>
									<Nav.Item className="mr-2">
										<Nav.Link 
											href="/user/transactions" 
											eventKey="/user/transactions"
										>Transactions</Nav.Link>
									</Nav.Item>

									<Nav.Item className="mx-2">
										<Nav.Link 
											href="/user/categories" 
											eventKey="/user/categories"
										>Categories</Nav.Link>
									</Nav.Item>

									<Nav.Item className="mx-2"> 
										<Nav.Link 
											href="/user/charts/monthly-income" 
											eventKey="/user/charts/monthly-income"
										>Monthly Income</Nav.Link>
									</Nav.Item>

									<Nav.Item className="mx-2"> 
										<Nav.Link 
											href="/user/charts/monthly-expense" 
											eventKey="/user/charts/monthly-expense"
										>Monthly Expense</Nav.Link>
									</Nav.Item>

									<Nav.Item className="mx-2"> 
										<Nav.Link 
											href="/user/charts/balance-trend" 
											eventKey="/user/charts/balance-trend"
										>Trend</Nav.Link>
									</Nav.Item>

									<Nav.Item className="mx-2"> 
										<Nav.Link 
											href="/user/charts/category-breakdown" 
											eventKey="/user/charts/category-breakdown"
										>Breakdown</Nav.Link>
									</Nav.Item>

									<Nav.Item className="ml-2"> 
										<Nav.Link 
											href="/about" 
											eventKey="/about"
										>About</Nav.Link>
									</Nav.Item>

									<Nav.Item className="ml-2"> 
										<Nav.Link 
											href="/instructions" 
											eventKey="/instructions"
										>INSTRUCTIONS</Nav.Link>
									</Nav.Item>
							</Nav>
							<Nav className="d-flex justify-content-center align-items-center">
								<Nav.Item>
									<Nav.Link href="/logout" className="nav-link-logout">Log Out</Nav.Link>
								</Nav.Item>
							</Nav>
						</Fragment>
						:
						<Nav className="d-flex justify-content-center align-items-center nav-no-logout" style={{ flex: 1}}>
							<Nav.Link href="/login" >Login</Nav.Link>
							<Nav.Link href="/register" >Register</Nav.Link>
							<Nav.Link href="/about" >About</Nav.Link>
							<Nav.Link href="/instructions" >INSTRUCTIONS</Nav.Link>
						</Nav>
					}
				</Navbar.Collapse>
			</Navbar>
		)
}