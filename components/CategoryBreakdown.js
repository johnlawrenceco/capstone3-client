import { useState, useEffect } from 'react'
import { Pie } from 'react-chartjs-2'
import { colorRandomizer } from '../helpers/colorRandomizer'

export default function PieChart({rawData}) {
	console.log(rawData)
	const [categoryName, setCategoryName] = useState([]) 
	const [amount, setAmount] = useState([]) 
	const [bgColors, setBgColors] = useState([]) 

	useEffect(() => {
		setCategoryName(rawData.map(element => element.categoryName))
		
	}, [rawData])

	useEffect(() => {
		setAmount(rawData.map(element => element.amount))

	}, [rawData])

	useEffect(() => {
		setBgColors(categoryName.map(() => `#${colorRandomizer()}`))
	}, [categoryName])

	console.log(amount)

		const data = {

			labels: categoryName,
			datasets: [{
				data: amount, 
				backgroundColor: bgColors,
				hoverBackgroundColor: bgColors
			}]
		}

		return (
			<Pie data={data}/>
		)

}
